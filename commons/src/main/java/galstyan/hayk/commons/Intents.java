package galstyan.hayk.commons;


import android.content.Intent;


public class Intents {


	public static Intent picker(String... mimeTypes) {
		Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);

		if (mimeTypes == null || mimeTypes.length == 0)
			intent.setType("*/*");
		else if (mimeTypes.length == 1)
			intent.setType(mimeTypes[0]);
		else {
			intent.setType("*/*");
			intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
		}

		return intent;
	}
}
