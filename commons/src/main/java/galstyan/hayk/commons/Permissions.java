package galstyan.hayk.commons;


import android.app.Activity;
import android.content.pm.PackageManager;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


public class Permissions {

	private static final AtomicInteger ID = new AtomicInteger(666);
	private final int mInstanceId = ID.getAndIncrement();

	@NonNull
	private final Activity mActivity;
	@NonNull
	private final String mPermission;
	@NonNull
	private final OnGrantedListener mOnGrantedListener;
	@Nullable
	private final OnShouldShowRationaleListener mOnShouldShowRationaleListener;
	@Nullable
	private final OnDeniedListener mOnDeniedListener;

	private boolean mRationaleShown;


	public interface OnGrantedListener {
		void onPermissionGranted();
	}

	public interface OnShouldShowRationaleListener {
		void onShouldShowRationale();
	}

	public interface OnDeniedListener {
		void onPermissionDenied();
	}


	public static class Builder {
		private Activity mActivity;
		private OnGrantedListener mOnGrantedListener;
		private OnShouldShowRationaleListener mOnShouldShowRationaleListener;
		private OnDeniedListener mOnDeniedListener;
		private String mPermission;

		public Builder(Activity activity) {
			mActivity = activity;
		}

		public Builder withPermission(String permission) {
			mPermission = permission;
			return this;
		}


		public Builder withOnGrantedListener(OnGrantedListener onGrantedListener) {
			mOnGrantedListener = onGrantedListener;
			return this;
		}

		public Builder withOnShouldShowRationaleListener(OnShouldShowRationaleListener onShouldShowRationaleListener) {
			mOnShouldShowRationaleListener = onShouldShowRationaleListener;
			return this;
		}

		public Builder withOnDeniedListener(OnDeniedListener onDeniedListener) {
			mOnDeniedListener = onDeniedListener;
			return this;
		}

		public Permissions build() {
			return new Permissions(
					mActivity,
					mPermission,
					mOnGrantedListener,
					mOnShouldShowRationaleListener,
					mOnDeniedListener
			);
		}
	}


	private Permissions(@NonNull Activity activity,
						@NonNull String permission,
						@NonNull OnGrantedListener onGrantedListener,
						@Nullable OnShouldShowRationaleListener onShouldShowRationaleListener,
						@Nullable OnDeniedListener onDeniedListener) {

		mActivity = Objects.requireNonNull(activity);
		mPermission = Objects.requireNonNull(permission);

		mOnGrantedListener = Objects.requireNonNull(onGrantedListener);

		mOnShouldShowRationaleListener = onShouldShowRationaleListener;
		mOnDeniedListener = onDeniedListener;
	}


	public Permissions check() {
		if (isGranted(mPermission))
			mOnGrantedListener.onPermissionGranted();
		else {
			if (mOnShouldShowRationaleListener != null
					&& shouldShowRationale(mPermission)
					&& !mRationaleShown) {

				mRationaleShown = true;
				mOnShouldShowRationaleListener.onShouldShowRationale();
			} else requestPermission(mPermission);
		}

		return this;
	}


	// call this in onRequestPermissionsResult
	public void handleRequestPermissionsResult(int requestCode,
											   @NonNull String[] permissions,
											   @NonNull int[] grantResults) {

		if (requestCode != mInstanceId) return;

		if (isGranted(grantResults[0]))
			mOnGrantedListener.onPermissionGranted();
		else if (mOnDeniedListener != null)
			mOnDeniedListener.onPermissionDenied();
	}


	public void requestPermission(String permission) {
		ActivityCompat.requestPermissions(mActivity, new String[]{permission}, mInstanceId);
	}


	public void requestPermission(String[] permissions) {
		ActivityCompat.requestPermissions(mActivity, permissions, mInstanceId);
	}


	public boolean shouldShowRationale(String permission) {
		return ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission);
	}


	public boolean isGranted(String permission) {
		return PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(mActivity, permission);
	}

	public static boolean isGranted(Activity activity, String permission) {
		return PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(activity, permission);
	}

	public boolean isGranted(int result) {
		return PackageManager.PERMISSION_GRANTED == result;
	}
}
