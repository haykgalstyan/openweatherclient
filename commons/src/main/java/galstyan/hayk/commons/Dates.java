package galstyan.hayk.commons;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;


public class Dates {


	public static class Formatter {
		private final Locale mLocale;
		private Date mDate;
		private String mFormat;

		public Formatter() {
			mLocale = Locale.getDefault();
		}

		public Formatter(Locale locale) {
			mLocale = Objects.requireNonNull(locale);
		}

		public Formatter from(Date date) {
			mDate = Objects.requireNonNull(date);
			return this;
		}

		public Formatter from(String dateString, String inParseFormat) {
			Objects.requireNonNull(dateString);
			Objects.requireNonNull(inParseFormat);
			try {
				mDate = new SimpleDateFormat(inParseFormat, mLocale).parse(dateString);
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
			return this;
		}

		public Formatter to(String outFormat) {
			mFormat = Objects.requireNonNull(outFormat);
			return this;
		}

		public Formatter to(Date date, String outFormat) {
			mDate = Objects.requireNonNull(date);
			mFormat = Objects.requireNonNull(outFormat);
			return this;
		}

		public String get() {
			return new SimpleDateFormat(mFormat, mLocale).format(mDate);
		}
	}


	public static Date now() {
		return new Date();
	}


	public static Date today() {
		 Calendar c = Calendar.getInstance();
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.HOUR_OF_DAY, 0);
		return c.getTime();
	}


	public static Date tomorrow() {
		return dateDaysFromNow(1);
	}


	public static Date dateDaysFromNow(int days) {
		 Calendar c = Calendar.getInstance();
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.HOUR_OF_DAY, 0);

		c.add(Calendar.DAY_OF_YEAR, days);

		return c.getTime();
	}
}
