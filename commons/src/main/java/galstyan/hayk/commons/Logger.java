package galstyan.hayk.commons;

import android.content.Intent;
import android.os.Bundle;
import android.util.Printer;

import java.util.List;
import java.util.Map;


/**
 * Created by haykgalstyan on 1/19/18.
 */


@SuppressWarnings({"WeakerAccess", "SameParameterValue"})
public class Logger implements Printer {


	private static final String LOGGER_KEY = "############ LOGGER ############ -> ";


	/***
	 *	Log Objects
	 */
	public static void log(Object object) {
		log("", object);
	}

	public static void log(String key, Object object) {
		System.out.println(LOGGER_KEY + " " + key + " : " + object);
	}


	/***
	 *	Log arrays
	 */
	public static void log(Object[] array) {
		log("", array);
	}

	public static void log(String key, Object[] array) {
		for (Object object : array) Logger.log(key, object);
	}


	/***
	 *	Log Maps
	 */
	public static void log(Map map) {
		log("", map);
	}

	public static void log(String key, Map map) {
		for (Object object : map.entrySet()) {
			Map.Entry entry = (Map.Entry) object;
			Logger.log(key, " K: " + entry.getKey() + ", V: " + entry.getValue());
		}
	}


	/***
	 *	Log Lists
	 */
	public static void log(List list) {
		log("", list);
	}

	public static void log(String key, List list) {
		for (Object object : list) Logger.log(key, object);
	}


	/***
	 *	Log Intents and Bundles
	 */
	public static void log(Intent intent) {
		log("", intent.getExtras());
	}

	public static void log(String key, Intent intent) {
		log(key, intent.getExtras());
	}

	public static void log(Bundle bundle) {
		if (bundle != null) log("", bundle);
	}

	public static void log(String key, Bundle bundle) {
		if (bundle != null)
			for (String k : bundle.keySet()) {
				Object v = bundle.get(k);
				if (v != null)
					log(key, String.format("%s %s (%s)", k, v.toString(), v.getClass().getName()));
			}
	}


	public static String getCallerClassName() {
		StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
		for (int i = 1; i < stElements.length; i++) {
			StackTraceElement ste = stElements[i];
			if (!ste.getClassName().equals(Logger.class.getName()) && ste.getClassName().indexOf("java.lang.Thread") != 0)
				return ste.getClassName();
		}
		return null;
	}


	public static String getCallerCallerClassName() {
		StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
		String callerClassName = null;
		for (int i = 1; i < stElements.length; i++) {
			StackTraceElement ste = stElements[i];
			if (!ste.getClassName().equals(Logger.class.getName()) && ste.getClassName().indexOf("java.lang.Thread") != 0)
				if (callerClassName == null) callerClassName = ste.getClassName();
				else if (!callerClassName.equals(ste.getClassName())) return ste.getClassName();
		}
		return null;
	}


	@Override
	public void println(String x) {
		Logger.log(x);
	}
}
