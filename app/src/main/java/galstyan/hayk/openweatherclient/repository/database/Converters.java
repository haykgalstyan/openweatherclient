package galstyan.hayk.openweatherclient.repository.database;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Date;
import java.util.List;
import java.util.Map;

import androidx.room.TypeConverter;
import galstyan.hayk.openweatherclient.model.Location;


public class Converters {


	@TypeConverter
	public static String locationToString(Location l) {
		return l == null ? null : new Gson().toJson(l, new TypeToken<Location>() {}.getType());
	}


	@TypeConverter
	public static Location stringToLocation(String ls) {
		return ls == null ? null : new Gson().fromJson(ls, new TypeToken<Location>() {}.getType());
	}


	@TypeConverter
	public Date fromTimestamp(Long value) {
		return value == null ? null : new Date(value);
	}


	@TypeConverter
	public Long dateToTimestamp(Date date) {
		if (date == null) {
			return null;
		} else {
			return date.getTime();
		}
	}


	@TypeConverter
	public static String mapToString(Map<String, String> value) {
		return value == null ? null : new Gson().toJson(value);
	}


	@TypeConverter
	public static Map<String, String> fromMap(String value) {
		return value == null ? null : new Gson().fromJson(value, Map.class);
	}


	@TypeConverter
	public static String stringListIn(List<String> value) {
		return value == null ? null : new Gson().toJson(value);
	}


	@TypeConverter
	public static List<String> stringListOut(String value) {
		return value == null ? null : new Gson().fromJson(value, List.class);
	}


	@TypeConverter
	public static String LongArrayToString(long[] value) {
		return value == null ? null : new Gson().toJson(value);
	}


	@TypeConverter
	public static long[] fromLongArray(String value) {
		return value == null ? null : new Gson().fromJson(value, long[].class);
	}


	@TypeConverter
	public static String LongListToString(List<Long> value) {
		return value == null ? null : new Gson().toJson(value, new TypeToken<List<Long>>() {}.getType());
	}


	@TypeConverter
	public static List<Long> fromLongList(String value) {
		return value == null ? null : new Gson().fromJson(value, new TypeToken<List<Long>>() {}.getType());
	}
}
