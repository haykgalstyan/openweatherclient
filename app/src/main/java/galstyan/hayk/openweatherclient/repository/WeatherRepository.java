package galstyan.hayk.openweatherclient.repository;


import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import galstyan.hayk.openweatherclient.model.enitity.Weather;
import galstyan.hayk.openweatherclient.repository.apiclient.ApiClient;
import galstyan.hayk.openweatherclient.repository.apiclient.ApiEndpoints;
import galstyan.hayk.openweatherclient.repository.apiclient.Responses;
import galstyan.hayk.openweatherclient.repository.database.AppDatabase;
import retrofit2.Response;


// todo: better api error handling

public class WeatherRepository {

	private static final WeatherRepository INSTANCE = new WeatherRepository();

	private static final ApiEndpoints API = ApiClient.get();
	private static final Executor EXECUTOR = Executors.newSingleThreadExecutor();


	public static WeatherRepository get() {
		return INSTANCE;
	}


	public LiveData<List<Weather>> loadFromDBObservable() {
		return AppDatabase.get().weatherModel().getAllObservable();
	}


	public LiveData<Weather> getWeatherObservable(String apiKey, String units,
												  double lat, double lon) {

		MutableLiveData<Weather> liveData = new MutableLiveData<>();
		EXECUTOR.execute(() -> liveData.postValue(getWeather(apiKey, units, lat, lon)));
		return liveData;
	}


	public LiveData<List<Weather>> getForecastObservable(String apiKey, String units,
														 double lat, double lon) {

		MutableLiveData<List<Weather>> listLiveData = new MutableLiveData<>();
		EXECUTOR.execute(() -> listLiveData.postValue(getForecast(apiKey, units, lat, lon)));
		return listLiveData;
	}


	@WorkerThread
	@Nullable
	public Weather getWeather(String apiKey, String units, double lat, double lon) {
		try {
			Response<Responses.Pojo.WeatherData> response =
					API.getWeather(apiKey, units, lat, lon)
							.execute();

			if (!response.isSuccessful()) throw new Exception("Something Went Wrong");

			Responses.Pojo.WeatherData data = response.body();
			return Responses.Factory.from(data);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}


	@WorkerThread
	@Nullable
	public List<Weather> getForecast(String apiKey, String units, double lat, double lon) {
		try {
			Response<Responses.Pojo.Forecast> response =
					API.getForecast(apiKey, units, lat, lon).execute();

			if (!response.isSuccessful()) throw new Exception("Something Went Wrong");

			Responses.Pojo.Forecast data = response.body();
			return Responses.Factory.from(data);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}

