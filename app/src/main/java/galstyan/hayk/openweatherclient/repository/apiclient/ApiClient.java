package galstyan.hayk.openweatherclient.repository.apiclient;

import android.util.Log;

import java.io.IOException;
import java.net.URLDecoder;

import androidx.annotation.Nullable;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {


	private static final String LOG_TAG = ApiClient.class.getSimpleName();

	private static final ApiEndpoints ENDPOINTS = create();


	public static ApiEndpoints get() {
		return ENDPOINTS;
	}


	private static ApiEndpoints create() {
		return new Retrofit.Builder()
				.baseUrl(ApiEndpoints.BASE_URL)
				.addConverterFactory(createConverterFactory())
				.client(createHttpClient())
				.build()
				.create(ApiEndpoints.class);
	}


	private static Converter.Factory createConverterFactory() {
		return GsonConverterFactory.create();
	}


	private static OkHttpClient createHttpClient() {
		return new OkHttpClient.Builder()
				.addInterceptor(createLogInterceptor())
				.build();
	}


	private static Interceptor createLogInterceptor() {
		return chain -> {
			Response response = chain.proceed(chain.request().newBuilder().build());
			Log.d(LOG_TAG, "##############");
			Log.d(LOG_TAG, "Request Url: "
					+ URLDecoder.decode(response.request().url().toString(), "UTF-8"));
			Log.d(LOG_TAG, "Request Body: "
					+ bodyToString(response.request().body()));
			Log.d(LOG_TAG, "Response Body: "
					+ response.peekBody(Long.MAX_VALUE).string());
			Log.d(LOG_TAG, "##############");
			return response;
		};
	}


	@Nullable
	private static String bodyToString(final RequestBody request) {
		try {
			final Buffer b = new Buffer();
			if (request != null)
				request.writeTo(b);
			else
				return "";
			return b.readUtf8();
		} catch (final IOException e) {
			return null;
		}
	}
}
