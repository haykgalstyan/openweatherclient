package galstyan.hayk.openweatherclient.repository.sync;


import java.util.concurrent.TimeUnit;

import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;

import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;


public class WeatherSyncWorkManager {


	private static final String TAG = WeatherSyncWorkManager.class.getSimpleName();
	private static final long PERIOD_MINS_DEFAULT = 15;


	public static void launchIfNeeded(Constraints constraints) {
		launchIfNeeded(constraints, PERIOD_MINS_DEFAULT);
	}


	public static void launchIfNeeded(Constraints constraints, long periodMinutes) {
		WorkManager
				.getInstance()
				.enqueueUniquePeriodicWork(
						TAG, ExistingPeriodicWorkPolicy.KEEP,
						new PeriodicWorkRequest
								.Builder(WeatherSyncWork.class, periodMinutes, TimeUnit.MINUTES)
								.setConstraints(constraints)
								.build()
				);
	}
}