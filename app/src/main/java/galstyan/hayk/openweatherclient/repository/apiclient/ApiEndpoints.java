package galstyan.hayk.openweatherclient.repository.apiclient;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiEndpoints {
	String BASE_URL = "https://api.openweathermap.org/";
	String API = "data/2.5/";
	String ICONS = "img/w/";
	String FORECAST = API + "forecast";
	String WEATHER = API + "weather";

	String KEY_LAT = "lat";
	String KEY_LON = "lon";
	String KEY_UNITS = "units";
	String KEY_API = "appid";



	@GET(FORECAST)
	Call<Responses.Pojo.Forecast> getForecast(
			@Query(KEY_API) String apiKey,
			@Query(KEY_UNITS) String units,
			@Query(KEY_LAT) double lat,
			@Query(KEY_LON) double lon
	);


	@GET(WEATHER)
	Call<Responses.Pojo.WeatherData> getWeather(
			@Query(KEY_API) String apiKey,
			@Query(KEY_UNITS) String units,
			@Query(KEY_LAT) double lat,
			@Query(KEY_LON) double lon
	);

}