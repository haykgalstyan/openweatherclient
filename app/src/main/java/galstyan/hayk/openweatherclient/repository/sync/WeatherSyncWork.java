package galstyan.hayk.openweatherclient.repository.sync;

import android.content.Context;

import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import galstyan.hayk.openweatherclient.Config;
import galstyan.hayk.openweatherclient.model.Location;
import galstyan.hayk.openweatherclient.model.enitity.Weather;
import galstyan.hayk.openweatherclient.prefs.Keys;
import galstyan.hayk.openweatherclient.prefs.Preferencer;
import galstyan.hayk.openweatherclient.repository.WeatherRepository;
import galstyan.hayk.openweatherclient.repository.database.AppDatabase;
import galstyan.hayk.openweatherclient.repository.database.dao.WeatherDao;


public class WeatherSyncWork extends Worker {


	private final String mApiKey;
	private final String mUnits;


	public WeatherSyncWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
		super(context, workerParams);
		mApiKey = Config.API_KEY_OPEN_WEATHER;
		mUnits = "metric";
	}

	// fixme: sorry, no time for injections


	@NonNull
	@Override
	public Result doWork() {
		Result result = Result.success();

		Location l = Preferencer.load(Keys.LAST_KNOWN_LOCATION, Location.class);

		if (l != null) {
			WeatherRepository repository = WeatherRepository.get();
			WeatherDao dao = AppDatabase.get().weatherModel();

			Weather weather = repository.getWeather(mApiKey, mUnits, l.getLat(), l.getLng());
			List<Weather> forecast = repository.getForecast(mApiKey, mUnits, l.getLat(), l.getLng());

			dao.truncate();
			dao.save(weather);
			dao.save(forecast);
		}

		return result;
	}
}
