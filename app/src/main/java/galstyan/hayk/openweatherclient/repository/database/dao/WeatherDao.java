package galstyan.hayk.openweatherclient.repository.database.dao;


import java.util.List;

import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import galstyan.hayk.openweatherclient.model.enitity.Weather;


@Dao
public interface WeatherDao extends BaseDao<Weather> {


	String TABLE = "weather";


	@Query("SELECT * FROM " + TABLE + " WHERE mDateTime IS :dateTime")
	LiveData<Weather> getObservable(final long dateTime);


	@Query("SELECT * FROM " + TABLE)
	LiveData<List<Weather>> getAllObservable();


	@WorkerThread
	@Query("SELECT * FROM " + TABLE + " WHERE mDateTime IS :dateTime")
	Weather get(final long dateTime);


	@WorkerThread
	@Query("SELECT * FROM " + TABLE)
	List<Weather> getAll();


	@Query("DELETE FROM " + TABLE)
	void truncate();
}