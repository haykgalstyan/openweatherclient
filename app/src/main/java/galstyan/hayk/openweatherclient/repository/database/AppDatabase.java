package galstyan.hayk.openweatherclient.repository.database;

import android.app.Application;


import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import galstyan.hayk.openweatherclient.model.enitity.Weather;
import galstyan.hayk.openweatherclient.repository.database.dao.WeatherDao;


@Database(
		entities = {
				Weather.class
		},
		version = 1,
		exportSchema = false
)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {


	private static AppDatabase sInstance;
	private static final String DB = "galstyan.hayk.openweatherclient.db";


	public abstract WeatherDao weatherModel();


	public static void init(final Application application) {
		sInstance = Room.databaseBuilder(application, AppDatabase.class, DB).build();
	}


	public static synchronized AppDatabase get() {
		return sInstance;
	}
}