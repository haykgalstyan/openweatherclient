package galstyan.hayk.openweatherclient.repository.database.dao;


import java.util.List;

import androidx.annotation.WorkerThread;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Update;

import static androidx.room.OnConflictStrategy.REPLACE;


@SuppressWarnings("unchecked")


@Dao
public interface BaseDao<T> {


	@WorkerThread
	@Insert(onConflict = REPLACE)
	void save(T item);


	@WorkerThread
	@Insert(onConflict = REPLACE)
	void save(List<T> items);


	@WorkerThread
	@Update
	void update(T item);

	@WorkerThread
	@Update
	void update(List<T> items);


	@WorkerThread
	@Delete
	void remove(T item);

	@WorkerThread
	@Delete
	void remove(List<T> items);
}
