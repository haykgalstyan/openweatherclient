package galstyan.hayk.openweatherclient.repository.apiclient;


import java.util.ArrayList;
import java.util.List;

import galstyan.hayk.openweatherclient.model.enitity.Weather;


public class Responses {


	public static class Factory {

		public static Weather from(final Pojo.WeatherData weatherData) {
			Pojo.Weather w = weatherData.weather.get(0);
			String imageUrl = ApiEndpoints.BASE_URL + ApiEndpoints.ICONS + w.icon;

			return new Weather.Builder()
					.setTemp(weatherData.main.temp)
					.setTempMin(weatherData.main.temp_min)
					.setTempMax(weatherData.main.temp_max)
					.setHumidity(weatherData.main.humidity)
					.setPressure(weatherData.main.pressure)
					.setDescription(w.description)
					.setIconUrl(imageUrl)
					.setDateTime(weatherData.dt * 1000)
					.setPlace(weatherData.name)
					.build();
		}

		public static List<Weather> from(final Pojo.Forecast forecast) {
			List<Weather> items = new ArrayList<>();

			for (Pojo.WeatherData weatherData : forecast.list) {
				Pojo.Weather w = weatherData.weather.get(0);
				String imageUrl = ApiEndpoints.BASE_URL + ApiEndpoints.ICONS + w.icon;

				Weather weather = new Weather.Builder()
						.setTemp(weatherData.main.temp)
						.setTempMin(weatherData.main.temp_min)
						.setTempMax(weatherData.main.temp_max)
						.setHumidity(weatherData.main.humidity)
						.setPressure(weatherData.main.pressure)
						.setDescription(w.description)
						.setIconUrl(imageUrl)
						.setDateTime(weatherData.dt * 1000)
						.setPlace(forecast.city.name)
						.build();

				items.add(weather);
			}

			return items;
		}
	}


	public static class Pojo {

		public class Forecast {
			public String cod;
			List<WeatherData> list;
			City city;
		}

		public class WeatherData {
			long dt;
			Main main;
			List<Weather> weather;
			String name;
		}

		class Weather {
			String description;
			String icon;
		}

		class Main {
			double temp;
			double temp_min;
			double temp_max;
			double pressure;
			int humidity;
		}

		class City {
			String name;
		}
	}
}
