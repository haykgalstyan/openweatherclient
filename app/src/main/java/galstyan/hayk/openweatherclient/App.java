package galstyan.hayk.openweatherclient;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ProcessLifecycleOwner;


public class App extends Application implements DefaultLifecycleObserver {


	private static boolean sIsForeground;


	@Override
	public void onCreate() {
		super.onCreate();

		ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

		Config.init(this);
	}

	public static boolean isForeground() {
		return sIsForeground;
	}

	@Override
	public void onStart(@NonNull final LifecycleOwner owner) {
		sIsForeground = true;
	}

	@Override
	public void onStop(@NonNull final LifecycleOwner owner) {
		sIsForeground = false;
	}
}