package galstyan.hayk.openweatherclient.imageloading;


import android.widget.ImageView;


public interface ImageLoader {

	<T extends ImageView> void loadInto(final T imageView, final String url);

	<T extends ImageView> void loadInto(final T imageView, final String url, final int placeHolderResId);
}
