package galstyan.hayk.openweatherclient.imageloading;


public class ImageLoaderProvider {


	private static ImageLoader sLoader;


	// todo: do a real factory :D

	public static ImageLoader get() {
		if (sLoader == null)
			sLoader = new PicassoLoader();
		return sLoader;
	}

	private ImageLoaderProvider() {}
}
