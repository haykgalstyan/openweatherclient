package galstyan.hayk.openweatherclient.imageloading;


import android.widget.ImageView;

import com.squareup.picasso.Picasso;


class PicassoLoader implements ImageLoader {


	private final Picasso mPicasso = Picasso.get();


	@Override
	public <T extends ImageView> void loadInto(final T imageView, final String url) {
		mPicasso.load(url)
				.into(imageView);
	}


	@Override
	public <T extends ImageView> void loadInto(final T imageView, final String url, final int placeHolderResId) {
		mPicasso.load(url)
				.placeholder(placeHolderResId)
				.into(imageView);
	}
}
