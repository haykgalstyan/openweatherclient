package galstyan.hayk.openweatherclient;


import android.app.Application;

import galstyan.hayk.openweatherclient.prefs.Preferencer;
import galstyan.hayk.openweatherclient.repository.database.AppDatabase;


public final class Config {

	public static final String API_KEY_OPEN_WEATHER = "82d81c226c9f6936d4d57e42ca874f1c";

	static void init(final Application application) {
		AppDatabase.init(application);
		Preferencer.init(application);
	}
}