package galstyan.hayk.openweatherclient.viewmodel;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import galstyan.hayk.openweatherclient.model.enitity.Weather;
import galstyan.hayk.openweatherclient.repository.WeatherRepository;


public class WeatherViewModel extends ViewModel {

	private static final WeatherRepository REPOSITORY = WeatherRepository.get();

	private final String mApiKey;
	private final String mUnits;


	private LiveData<List<Weather>> mCached;


	public WeatherViewModel(String apiKey, String units) {
		mApiKey = apiKey;
		mUnits = units;
	}


	public LiveData<List<Weather>> getCachedData() {
		if (mCached == null)
			mCached = REPOSITORY.loadFromDBObservable();
		return mCached;
	}


	public LiveData<List<Weather>> getForecast(double lat, double lon) {
		return REPOSITORY.getForecastObservable(mApiKey, mUnits, lat, lon);
	}


	public LiveData<Weather> getWeather(double lat, double lon) {
		return REPOSITORY.getWeatherObservable(mApiKey, mUnits, lat, lon);
	}
}