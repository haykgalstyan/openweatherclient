package galstyan.hayk.openweatherclient.viewmodel;

import java.util.concurrent.Executor;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;


public class WeatherViewModelFactory implements ViewModelProvider.Factory {

	private final String mApiKey;
	private final Units mUnits;


	public enum Units {
		METRIC("metric"), IMPERIAL("imperial");

		private final String mVal;

		Units(String val) {
			mVal = val;
		}

		private String getVal() {
			return mVal;
		}
	}

	public WeatherViewModelFactory(String apiKey, Units units) {
		mApiKey = apiKey;
		mUnits = units;
	}


	@NonNull
	@Override
	public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
		return (T) new WeatherViewModel(mApiKey, mUnits.mVal);
	}
}
