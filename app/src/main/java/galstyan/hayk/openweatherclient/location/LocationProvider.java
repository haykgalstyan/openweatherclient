package galstyan.hayk.openweatherclient.location;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;

import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;
import androidx.core.content.ContextCompat;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;


@SuppressLint("MissingPermission")
public class LocationProvider {

	private static final long DEFAULT_LOCATION_REQUEST_INTERVAL = 5 * 60 * 1000;
	private static final long DEFAULT_LOCATION_REQUEST_INTERVAL_FAST = DEFAULT_LOCATION_REQUEST_INTERVAL;
	private static final int REQUEST_CHECK_SETTINGS = 34;


	private final Activity mActivity;
	private final LocationListener mLocationListener;
	private final LocationCanceledListener mCanceledListener;

	@Nullable
	private Location mLocation;
	private final FusedLocationProviderClient mLocationClient;
	private final LocationRequest mLocationRequest;
	private LocationCallback mLocationCallback;


	public interface LocationListener {
		void onNewLocation(@Nullable Location location);
	}

	public interface LocationCanceledListener {
		void onLocationCanceled();
	}


	@Nullable
	private Location getLocation() {
		return mLocation == null ? null : new Location(mLocation);
	}


	public void handleActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		if (requestCode != REQUEST_CHECK_SETTINGS) return;

		if (resultCode == Activity.RESULT_OK) {
			requestLocationUpdates();
		} else
			mCanceledListener.onLocationCanceled();
	}


	public void stopUpdates() {
		mLocationClient.removeLocationUpdates(mLocationCallback);
	}


	public static class Builder {
		private Activity mActivity;
		private Long mUpdateInterval;
		private LocationListener mLocationListener;
		private LocationCanceledListener mCanceledListener;

		public Builder(Activity activity) {
			mActivity = activity;
		}

		public Builder withUpdateInterval(Long updateInterval) {
			mUpdateInterval = updateInterval;
			return this;
		}

		public Builder withLocationListener(LocationListener locationListener) {
			mLocationListener = locationListener;
			return this;
		}

		public Builder withCanceledListener(LocationCanceledListener canceledListener) {
			mCanceledListener = canceledListener;
			return this;
		}


		/**
		 * @throws SecurityException If missing location permissions
		 */
		@RequiresPermission(allOf = {ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION})
		public LocationProvider build() {
			return new LocationProvider(mActivity, mUpdateInterval, mLocationListener, mCanceledListener);
		}
	}


	private LocationProvider(Activity activity,
	                         final Long updateInterval,
	                         LocationListener listener,
	                         LocationCanceledListener canceledListener) {

		checkPermission(activity);

		mActivity = Objects.requireNonNull(activity);
		mLocationListener = Objects.requireNonNull(listener);
		mCanceledListener = canceledListener;

		mLocationClient = LocationServices.getFusedLocationProviderClient(activity);

		mLocationRequest = new LocationRequest()
				.setInterval(updateInterval != null ? updateInterval : DEFAULT_LOCATION_REQUEST_INTERVAL)
				.setFastestInterval(updateInterval != null ? updateInterval : DEFAULT_LOCATION_REQUEST_INTERVAL_FAST)
				.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

		initLocation();
		handleSettings();
	}


	private void checkPermission(Context context) {
		if (ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
				&& ContextCompat.checkSelfPermission(context, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
			throw new SecurityException("Missing Permission");
	}


	private void initLocation() {
		mLocationClient.getLastLocation().addOnSuccessListener(this::onNewLocation);
		mLocationCallback = new LocationCallback() {
			@Override
			public void onLocationResult(LocationResult locationResult) {
				onNewLocation(locationResult.getLastLocation());
			}
		};
	}


	private void handleSettings() {
		LocationSettingsRequest.Builder settingsRequest = new LocationSettingsRequest.Builder()
				.addLocationRequest(mLocationRequest)
				.setAlwaysShow(true);

		LocationServices.getSettingsClient(mActivity)
				.checkLocationSettings(settingsRequest.build())
				.addOnSuccessListener(response -> requestLocationUpdates())
				.addOnFailureListener(this::onSettingsFailed);
	}


	private void requestLocationUpdates() {
		mLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
	}


	private void onSettingsFailed(Exception e) {
		if (e instanceof ResolvableApiException)
			try {
				((ResolvableApiException) e).startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
			} catch (IntentSender.SendIntentException sendError) {
				sendError.printStackTrace();
			}
	}


	private void onNewLocation(Location location) {
		mLocation = location;
		mLocationListener.onNewLocation(mLocation);
	}
}