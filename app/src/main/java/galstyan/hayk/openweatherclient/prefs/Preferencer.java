package galstyan.hayk.openweatherclient.prefs;

import android.app.Application;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import static android.content.Context.MODE_PRIVATE;


/**
 * Methods with no key param save a single object for a class with the class name.
 */

@SuppressWarnings("unchecked")
public final class Preferencer {


	private static final String PREFERENCES = "galstyan.hayk.openweatherclient.prefs";
	private static Application sApplication;


	public static void init(Application application) {
		sApplication = application;
	}


	public static SharedPreferences prefs() {
		return sApplication.getSharedPreferences(PREFERENCES, MODE_PRIVATE);
	}


	public static SharedPreferences.Editor edit() {
		return sApplication.getSharedPreferences(PREFERENCES, MODE_PRIVATE).edit();
	}


	public static void save(Object object) {
		save(object.getClass().getName(), object);
	}


	public static void save(String key, Object object) {
		sApplication.getSharedPreferences(PREFERENCES, MODE_PRIVATE)
				.edit().putString(key, new Gson().toJson(object))
				.apply();
	}


	public static <T> T load(Class typeClass) {
		return load(typeClass.getName(), typeClass);
	}


	public static <T> T load(String key, Class typeClass) {
		return new Gson().fromJson(sApplication.getSharedPreferences(PREFERENCES, MODE_PRIVATE)
				.getString(key, null), (Class<T>) typeClass);
	}


	public static void clear(Class typeClass) {
		clear(typeClass.getName());
	}


	public static void clear(String key) {
		sApplication.getSharedPreferences(PREFERENCES, MODE_PRIVATE)
				.edit().putString(key, null)
				.apply();
	}
}
