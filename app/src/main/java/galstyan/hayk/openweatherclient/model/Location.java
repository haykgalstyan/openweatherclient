package galstyan.hayk.openweatherclient.model;



import java.io.Serializable;


public class Location implements Serializable {

	private final double mLat, mLng;

	public Location(double lat, double lng) {
		mLat = lat;
		mLng = lng;
	}

	public double getLat() {
		return mLat;
	}

	public double getLng() {
		return mLng;
	}
}
