package galstyan.hayk.openweatherclient.model.enitity;


import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity
public class Weather {


	@PrimaryKey()
	private final long mDateTime;

	private final String mDescription;

	private final double
			mTemp,
			mTempMin,
			mTempMax;

	private final int mHumidity;

	private final double mPressure;

	private final String mIconUrl;


	private final String mPlace;


	public static class Builder {
		private String mDescription;
		private String mPlace;
		private double mTemp;
		private double mTempMin;
		private double mTempMax;
		private int mHumidity;
		private double mPressure;
		private String mIconUrl;
		private long mDateTime;

		public Builder setDescription(String description) {
			mDescription = description;
			return this;
		}

		public Builder setPlace(String place) {
			mPlace = place;
			return this;
		}

		public Builder setTemp(double temp) {
			mTemp = temp;
			return this;
		}

		public Builder setTempMin(double tempMin) {
			mTempMin = tempMin;
			return this;
		}

		public Builder setTempMax(double tempMax) {
			mTempMax = tempMax;
			return this;
		}

		public Builder setHumidity(int humidity) {
			mHumidity = humidity;
			return this;
		}

		public Builder setPressure(double pressure) {
			mPressure = pressure;
			return this;
		}

		public Builder setIconUrl(String iconUrl) {
			mIconUrl = iconUrl;
			return this;
		}

		public Builder setDateTime(long dateTime) {
			mDateTime = dateTime;
			return this;
		}

		public Weather build() {
			return new Weather(mDescription, mPlace, mTemp, mTempMin, mTempMax, mHumidity, mPressure, mIconUrl, mDateTime);
		}
	}


	public Weather(String description,
				   String place,
				   double temp,
				   double tempMin,
				   double tempMax,
				   int humidity,
				   double pressure,
				   String iconUrl,
				   long dateTime) {


		mDescription = description;
		mPlace = place;
		mTemp = temp;
		mTempMin = tempMin;
		mTempMax = tempMax;
		mHumidity = humidity;
		mPressure = pressure;
		mIconUrl = iconUrl;
		mDateTime = dateTime;
	}


	public String getDescription() {
		return mDescription;
	}

	public double getTemp() {
		return mTemp;
	}

	public double getTempMin() {
		return mTempMin;
	}

	public double getTempMax() {
		return mTempMax;
	}

	public int getHumidity() {
		return mHumidity;
	}

	public double getPressure() {
		return mPressure;
	}

	public String getIconUrl() {
		return mIconUrl;
	}

	public long getDateTime() {
		return mDateTime;
	}

	public String getPlace() {
		return mPlace;
	}


	@Override
	public String toString() {
		return "Weather{" +
				"mDateTime=" + mDateTime +
				", mDescription='" + mDescription + '\'' +
				", mTemp=" + mTemp +
				", mTempMin=" + mTempMin +
				", mTempMax=" + mTempMax +
				", mHumidity=" + mHumidity +
				", mPressure=" + mPressure +
				", mIconUrl='" + mIconUrl + '\'' +
				", mPlace='" + mPlace + '\'' +
				'}';
	}
}