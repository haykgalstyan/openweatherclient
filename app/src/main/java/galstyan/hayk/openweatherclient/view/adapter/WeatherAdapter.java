package galstyan.hayk.openweatherclient.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import galstyan.hayk.commons.Dates;
import galstyan.hayk.openweatherclient.R;
import galstyan.hayk.openweatherclient.imageloading.ImageLoader;
import galstyan.hayk.openweatherclient.model.enitity.Weather;


public class WeatherAdapter extends ListAdapter<Weather, WeatherAdapter.WeatherViewHolder> {

	private final LayoutInflater mLayoutInflater;
	private final ImageLoader mImageLoader;
	private final String mDateFormat;


	// todo: just a copy from the docs, write real stuff
	private static final DiffUtil.ItemCallback<Weather> DIFF_CALLBACK =
			new DiffUtil.ItemCallback<Weather>() {
				@Override
				public boolean areItemsTheSame(@NonNull Weather oldWeather, @NonNull Weather newWeather) {
					// Weather properties may have changed if reloaded from the DB, but ID is fixed
					return oldWeather.getDateTime() == newWeather.getDateTime();
				}

				@Override
				public boolean areContentsTheSame(@NonNull Weather oldWeather, @NonNull Weather newWeather) {
					// NOTE: if you use equals, your object must properly override Object#equals()
					// Incorrectly returning false here will result in too many animations.
					return oldWeather.equals(newWeather);
				}
			};


	public WeatherAdapter(Context context, ImageLoader imageLoader, String dateFormat) {
		super(DIFF_CALLBACK);
		mLayoutInflater = LayoutInflater.from(context);
		mImageLoader = imageLoader;
		mDateFormat = dateFormat;
	}


	@NonNull
	@Override
	public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = mLayoutInflater.inflate(R.layout.item_weather, parent, false);
		return new WeatherViewHolder(view);
	}


	@Override
	public void onBindViewHolder(WeatherViewHolder holder, int position) {
		holder.bindTo(getItem(position));
	}


	class WeatherViewHolder extends RecyclerView.ViewHolder {
		private final TextView mLabelDateTime;
		private final TextView mLabelTemp;
		private final ImageView mImageWeatherIcon;

		public WeatherViewHolder(@NonNull View itemView) {
			super(itemView);
			mLabelDateTime = itemView.findViewById(R.id.label_datetime);
			mLabelTemp = itemView.findViewById(R.id.label_temp);
			mImageWeatherIcon = itemView.findViewById(R.id.image_weather_icon);
		}

		private void bindTo(Weather item) {
			String dateTime = new Dates.Formatter()
					.from(new Date(item.getDateTime()))
					.to(mDateFormat)
					.get();

			mLabelDateTime.setText(dateTime);
			mLabelTemp.setText(mLabelTemp.getContext().getString(
					R.string.temp_decimal_celsius, Math.round(item.getTemp())));

			mImageLoader.loadInto(mImageWeatherIcon, item.getIconUrl());
		}
	}
}
