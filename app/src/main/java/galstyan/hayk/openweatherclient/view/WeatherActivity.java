package galstyan.hayk.openweatherclient.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import galstyan.hayk.commons.Permissions;
import galstyan.hayk.openweatherclient.Config;
import galstyan.hayk.openweatherclient.R;
import galstyan.hayk.openweatherclient.databinding.ActivityMainBinding;
import galstyan.hayk.openweatherclient.imageloading.ImageLoaderProvider;
import galstyan.hayk.openweatherclient.location.LocationProvider;
import galstyan.hayk.openweatherclient.model.enitity.Weather;
import galstyan.hayk.openweatherclient.prefs.Keys;
import galstyan.hayk.openweatherclient.prefs.Preferencer;
import galstyan.hayk.openweatherclient.repository.sync.WeatherSyncWorkManager;
import galstyan.hayk.openweatherclient.view.adapter.WeatherAdapter;
import galstyan.hayk.openweatherclient.viewmodel.WeatherViewModel;
import galstyan.hayk.openweatherclient.viewmodel.WeatherViewModelFactory;


/**
 * fixme:  NOTE! I would have used fragments if I had more time
 */
public class WeatherActivity extends AppCompatActivity {

	private ActivityMainBinding mViewBinder;
	private Permissions mPermissions;
	private LocationProvider mLocationProvider;
	private WeatherViewModel mWeatherViewModel;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mViewBinder = DataBindingUtil.setContentView(this, R.layout.activity_main);

		mPermissions = new Permissions.Builder(this)
				.withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
				.withOnGrantedListener(this::initLocation)
				.build()
				.check();

		mWeatherViewModel = ViewModelProviders.of(this, new WeatherViewModelFactory(
				Config.API_KEY_OPEN_WEATHER, WeatherViewModelFactory.Units.METRIC))
				.get(WeatherViewModel.class);
	}


	@SuppressLint("MissingPermission")
	private void initLocation() {
		mLocationProvider = new LocationProvider.Builder(this)
				.withLocationListener(this::onNewLocation)
				.build();
	}


	private void onNewLocation(final Location location) {
		if (location == null) return;

		Preferencer.save(Keys.LAST_KNOWN_LOCATION, location);

		WeatherSyncWorkManager.launchIfNeeded(
				new Constraints.Builder()
						.setRequiredNetworkType(NetworkType.CONNECTED)
						.build()
		);

		mWeatherViewModel
				.getCachedData()
				.observe(this, (data) -> {
					if (data == null || data.size() == 0) return;

					updateWeather(data.get(0));
					updateForecast(data.subList(1, data.size()));
				});

		mWeatherViewModel.getForecast(location.getLatitude(), location.getLongitude());
	}


	private void updateWeather(Weather weather) {
		if (weather == null) return;

		mViewBinder.labelPlace.setText(weather.getPlace());
		mViewBinder.labelWeatherTemp.setText(getString(R.string.temp_decimal_celsius, Math.round(weather.getTemp())));
		mViewBinder.labelWeatherDescription.setText(weather.getDescription());
	}


	private void updateForecast(List<Weather> forecast) {
		if (forecast == null) return;

		WeatherAdapter adapter = new WeatherAdapter(this, ImageLoaderProvider.get(), "dd hh:mm");
		mViewBinder.recyclerWeatherToday.setAdapter(adapter);
		adapter.submitList(forecast);
	}


	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (mLocationProvider != null)
			mLocationProvider.handleActivityResult(requestCode, resultCode, data);
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mLocationProvider != null)
			mLocationProvider.stopUpdates();
	}


	@Override
	public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		mPermissions.handleRequestPermissionsResult(requestCode, permissions, grantResults);
	}
}
